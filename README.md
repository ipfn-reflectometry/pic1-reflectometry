# PIC1-Reflectometry

Project for iteratively solving the inverse problem of finding the plasma density profile for a given set of measurements from an O-mode frequency-modulated continuous-wave reflectometer.
